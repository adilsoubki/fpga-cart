#include <gb/gb.h>
#include <gb/drawing.h>
#include <stdint.h>

#define RAMC_REG (*(__REG)0x0000)

void enable_ram()
{
    RAMC_REG = 0x0A;
}

void disable_ram()
{
    RAMC_REG = 0x00;
}

uint8_t read_ram(uint16_t address)
{
    enable_ram();
    uint8_t value = (*(__REG)address);
    disable_ram();
    return value;
}

void tone(uint8_t freq)
{
    // FF10 - NR10 - Channel 1 Sweep register (R/W)
    //
    // Bit 6-4 - Sweep Time
    // Bit 3   - Sweep Increase/Decrease
    //            0: Addition    (frequency increases)
    //            1: Subtraction (frequency decreases)
    // Bit 2-0 - Number of sweep shift (n: 0-7)
    NR10_REG = 0x00; // 0000 0000
    // FF11 - NR11 - Channel 1 Sound length/Wave pattern duty (R/W)
    //
    // Bit 7-6 - Wave Pattern Duty (Read/Write)
    // Bit 5-0 - Sound length data (Write Only) (t1: 0-63)
    //
    // Wave Duty:
    // 00: 12.5% ( _-------_-------_------- )
    // 01: 25%   ( __------__------__------ )
    // 10: 50%   ( ____----____----____---- ) (normal)
    // 11: 75%   ( ______--______--______-- )
    //
    // Sound Length = (64-t1)*(1/256) seconds The Length value is used only if Bit 6 in NR14 is set.
    NR11_REG = 0x40; // 0100 0000
    // FF12 - NR12 - Channel 1 Volume Envelope (R/W)
    //
    // Bit 7-4 - Initial Volume of envelope (0-0Fh) (0=No Sound)
    // Bit 3   - Envelope Direction (0=Decrease, 1=Increase)
    // Bit 2-0 - Number of envelope sweep (n: 0-7)
    //           (If zero, stop envelope operation.)
    NR12_REG = 0xFF; // 1111 1011
    // FF13 - NR13 - Channel 1 Frequency lo (Write Only)
    //
    // Lower 8 bits of 11 bit frequency (x). Next 3 bit are in NR14 ($FF14)
    NR13_REG = freq; // 0000 0000
    // FF14 - NR14 - Channel 1 Frequency hi (R/W)
    //
    // Bit 7   - Initial (1=Restart Sound)     (Write Only)`
    // Bit 6   - Counter/consecutive selection (Read/Write)`
    //           (1=Stop output when length in NR11 expires)`
    // Bit 2-0 - Frequency's higher 3 bits (x) (Write Only)`
    //
    // Frequency = 131072/(2048-x) Hz
    NR14_REG = 0xC3; // 1100 0011
}

void main()
{
    DISPLAY_ON;
    NR52_REG = 0x80;
    NR50_REG = 0x77;
    NR51_REG = 0xFF;

    while (1)
    {
        uint8_t freq = read_ram(0xA000);
        gotogxy(9, 8);
        gprintf("%x", freq);
        tone(freq);
        delay(100);
    }
}
