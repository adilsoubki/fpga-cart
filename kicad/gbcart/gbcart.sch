EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L gbcart:Gameboy_Cartridge_Connector CN1
U 1 1 5EBD0AFB
P 1250 2600
F 0 "CN1" H 1183 4325 50  0000 C CNN
F 1 "Gameboy_Cartridge_Connector" H 1183 4234 50  0000 C CNN
F 2 "gameboy:Cartridge" H 1650 3500 50  0001 C CNN
F 3 "" H 1650 3500 50  0001 C CNN
	1    1250 2600
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x16_Top_Bottom J1
U 1 1 5EBCF014
P 2750 1800
F 0 "J1" H 2800 2717 50  0000 C CNN
F 1 "Conn_02x16_Top_Bottom" H 2800 2626 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x16_P2.54mm_Vertical" H 2750 1800 50  0001 C CNN
F 3 "~" H 2750 1800 50  0001 C CNN
	1    2750 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 2500 3050 2500
Wire Wire Line
	3150 4100 3150 2500
Wire Wire Line
	3250 2400 3050 2400
Wire Wire Line
	3250 4000 3250 2400
Wire Wire Line
	3350 2300 3050 2300
Wire Wire Line
	3350 3900 3350 2300
Wire Wire Line
	3450 2200 3050 2200
Wire Wire Line
	3450 3800 3450 2200
Wire Wire Line
	3750 1900 3050 1900
Wire Wire Line
	3850 1800 3050 1800
Wire Wire Line
	3950 1700 3050 1700
Wire Wire Line
	4250 1400 3050 1400
Wire Wire Line
	4350 1300 3050 1300
Wire Wire Line
	4450 1200 3050 1200
Wire Wire Line
	4550 1100 3050 1100
Wire Wire Line
	4550 2700 4550 1100
Wire Wire Line
	4450 2800 4450 1200
Wire Wire Line
	4350 2900 4350 1300
Wire Wire Line
	4250 3000 4250 1400
Wire Wire Line
	4150 3100 4150 1500
Wire Wire Line
	4050 3200 4050 1600
Wire Wire Line
	3950 3300 3950 1700
Wire Wire Line
	3850 3400 3850 1800
Wire Wire Line
	3750 3500 3750 1900
Wire Wire Line
	3650 3600 3650 2000
Wire Wire Line
	3550 3700 3550 2100
Wire Wire Line
	4150 1500 3050 1500
Wire Wire Line
	3650 2000 3050 2000
Wire Wire Line
	3550 2100 3050 2100
Wire Wire Line
	4050 1600 3050 1600
Wire Wire Line
	1350 4100 3150 4100
Wire Wire Line
	1350 4000 3250 4000
Wire Wire Line
	1350 3900 3350 3900
Wire Wire Line
	1350 3800 3450 3800
Wire Wire Line
	1350 2700 4550 2700
Wire Wire Line
	1350 2800 4450 2800
Wire Wire Line
	1350 2900 4350 2900
Wire Wire Line
	1350 2600 2550 2600
Wire Wire Line
	1350 2500 2550 2500
Wire Wire Line
	1350 2400 2550 2400
Wire Wire Line
	1350 2300 2550 2300
Wire Wire Line
	1350 2200 2550 2200
Wire Wire Line
	1350 2100 2550 2100
Wire Wire Line
	1350 2000 2550 2000
Wire Wire Line
	1350 1900 2550 1900
Wire Wire Line
	1350 1800 2550 1800
Wire Wire Line
	1350 1700 2550 1700
Wire Wire Line
	1350 1600 2550 1600
Wire Wire Line
	1350 1500 2550 1500
Wire Wire Line
	1350 1400 2550 1400
Wire Wire Line
	1350 1300 2550 1300
Wire Wire Line
	1350 1200 2550 1200
Wire Wire Line
	1350 1100 2550 1100
Wire Wire Line
	1350 3700 3550 3700
Wire Wire Line
	1350 3600 3650 3600
Wire Wire Line
	1350 3500 3750 3500
Wire Wire Line
	1350 3400 3850 3400
Wire Wire Line
	1350 3300 3950 3300
Wire Wire Line
	1350 3200 4050 3200
Wire Wire Line
	1350 3100 4150 3100
Wire Wire Line
	1350 3000 4250 3000
Wire Wire Line
	1350 4200 3050 4200
Wire Wire Line
	3050 4200 3050 2600
$EndSCHEMATC
