#include "WProgram.h"

const uint8_t RX = 0;
const uint8_t TX = 1;
const uint8_t LED = 13;
const uint8_t POT = 32;

void print_binary(long int n) {
  byte numBits = 8;  // 2^numBits must be big enough to include the number n
  char b;
  // char c = ' ';   // delimiter character
  for (byte i = 0; i < numBits; i++) {
    // shift 1 and mask to identify each bit value
    b = (n & (1 << (numBits - 1 - i))) > 0 ? '1' : '0'; // slightly faster to print chars than ints (saves conversion)
    Serial.print(b);
    // if (i < (numBits - 1) && ((numBits-i - 1) % 4 == 0 )) Serial.print(c); // print a separator at every 4 bits
  }
}

void print_byte(uint8_t byte)
{
    Serial.print("    0b");
    print_binary(byte);
    Serial.print("    0x");
    Serial.print(byte, HEX);
    Serial.print("    ");
    Serial.println(byte, DEC);
}

void loop()
{
    //uint8_t byte;

    //if (Serial.available() > 0) {
    //    byte = Serial.read();
    //    Serial.print("TX:");
    //    print_byte(byte);
    //    Serial1.write(byte);
    //}

    //if (Serial1.available() > 0) {
    //    byte = Serial1.read();
    //    Serial.print("RX:");
    //    print_byte(byte);
    //}

    float raw = analogRead(POT);
    uint8_t value = floor((raw / 0x3FF) * 0xFF);
    Serial1.write(value);
}

void setup()
{
    Serial.begin(9600);
    Serial1.begin(9600);

    pinMode(LED, OUTPUT);
    digitalWrite(LED, 1);

    pinMode(POT, INPUT);
}

extern "C" int main(void)
{
    setup();
    while (1) {
        loop();
    }
}
