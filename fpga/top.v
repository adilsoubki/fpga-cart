module top
(
    // 16 address pins
    input  [15:0] address,

    // Data pins (bidirectional)
    inout [7:0] data,

    // Signals from cartridge
    input nWR,   // Write
    input nRD,   // Read
    input nCS,   // Chip-select

    // Output enable for data level shifter
    output OE,

    // 16 MHz clock from FPGA
    input clk_16mhz,

    // UART interface
    input uart_rx,
    output uart_tx
);

// Disable USB [https://discourse.tinyfpga.com/t/why-drive-usb-pull-up-resistor-to-0-to-disable-usb/1276/2]
assign pin_pu = 0;

reg[7:0] rom[0:32768];

// Read headers & ROM from files
initial
begin
    $readmemh("../rom/bin/rom.hex", rom, 0, 32768);
end

reg [7:0] data_out = 8'd0;
reg output_enable = 1'd0;

// Enable data output if required, otherwise high-impedance
assign data = output_enable ? data_out : 8'bzzzzzzzz;
assign OE = output_enable;

always @(posedge clk_100mhz) begin
    if (nRD || ~nWR || ~nCS) begin
        output_enable <= 1'd0;
    end else if (address < 16'h8000) begin
        data_out <= rom[address];
        output_enable <= 1'd1;
    end else if (address >= 16'hA000 && address <= 16'hBFFF) begin
        data_out <= ram; // 8'h69;
        output_enable <= 1'd1;
    end else begin
        output_enable <= 1'd0;
    end
end

// UART
wire [7:0] uart_rx_byte;
reg [7:0] uart_tx_byte;
wire uart_received;
reg uart_transmit = 1'b0;

uart #(
    .baud_rate(9600),
    .sys_clk_freq(100000000)
) uart0 (
    .clk(clk_100mhz),
    // .rst(),
    .rx(uart_rx),
    .tx(uart_tx),
    .transmit(uart_transmit),
    .tx_byte(uart_tx_byte),
    .received(uart_received),
    .rx_byte(uart_rx_byte),
    // .is_receiving(),
    // .is_transmitting(),
    .recv_error()
);

reg [7:0] ram;

always @(posedge clk_100mhz) begin
    if (uart_received) begin
        // Echo the input.
        uart_tx_byte <= uart_rx_byte;
        uart_transmit <= 1'b1;
        // Store it in a "ram" buffer.
        ram <= uart_rx_byte;
    end else begin
        uart_transmit <= 1'b0;
    end
end

// Generate 100mhz clock signal
wire clk_100mhz;

pll_100mhz pll(
    .clock_in(clk_16mhz),
    .clock_out(clk_100mhz),
    .locked()
);

endmodule

